import React from 'react';
import './styles';
import CounterApp from './CounterApp';
import TodoApp from './TodoApp'
import ProductApp from './ProductApp';


console.log("trial test");

const App = () => {
	return (
		<div>
			<CounterApp />
			<TodoApp />
			<ProductApp />
		</div>
	);
}; 

export default App;