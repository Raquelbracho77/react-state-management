import React from 'react';
import { useAppContext } from './AppContext';

export const Content = () => {
	let {theme} = useAppContext();

	return <div 
		style={{color: theme === 'dark' ? 'black' : 'green'}}
	>
		Here's our main content
	</div>;
};