import React from 'react';
import { Content } from './Content';
import{ Sidebar } from './Sidebar';
//import { ThemeControl } from './ThemeControl';
import { AppProvider } from './AppContext';

const App = () => {
	console.log("trial test")
	return (
		<AppProvider>
		<div>
			<Content />
			<Sidebar />
			
		</div>
		</AppProvider>
	);
}; 

export default App;