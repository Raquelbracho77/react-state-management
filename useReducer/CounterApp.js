import React, { useReducer } from 'react';

const typesCase = {
	increment: 'increment',
	decremet: 'decrement',
	reset: 'reset'
}

const reducer = (state, action) => {
	switch(action.type) {
		case typesCase.increment:
			return state +1;
		case typesCase.decrement:
			return state -1;
		case typesCase.reset:
			return 0;
		default:
			return state;
	}
}

const CounterApp = () => {
	//console.log("Initial function:",CounterApp);

	const [counter, dispatch] = useReducer(reducer, 0);
	//console.log("Hook:", useReducer)
	return (
		<div>
			<h1>Counter App</h1>
			<h2>Clicks: {counter}</h2>
			<button onClick={ () => dispatch( {type: typesCase.increment })}>
				Increment
			</button>
			<button onClick={() => dispatch({ type: typesCase.decrement })}>
				Decrement
			</button>
			<button onClick={() => dispatch({ type: typesCase.reset })}>
				Reset
			</button>
		</div>
	);
}

export default CounterApp;